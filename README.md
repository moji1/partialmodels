# README #


### Building ###

Edit pmd_helper/env.cmake, then 
```
cd pmd_helper
mkdir build && cd build
cmake ..
make
# or make parser/util/pmecore/..
```

### Directory Structure
```
pmd_helper --> Source code of the debugging probe and evaluation test scripts
transformation --> Source of the model refinement
tools --> tools required to build and execute the partial models

```
### Demonstration Video
[A demonstration video of PMEXec features for execution of partial models](https://youtu.be/BRKsselcMnc)
