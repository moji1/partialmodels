package ca.jahed.papyrusrt.partialmodels;


public interface PapyrusrtLaunchConfigurationAttributes {

	public static final String ATTR_MODEL_FILE = "ca.jahed.papyrusrt.mcute.modelFile";
	public static final String ATTR_SCRIPT_FILE = "ca.jahed.papyrusrt.mcute.scriptFile";
	public static final String ATTR_MODE = "ca.jahed.papyrusrt.mcute.mode";
}
