package ca.jahed.papyrusrt.partialmodels;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchConfigurationWorkingCopy;
import org.eclipse.debug.ui.AbstractLaunchConfigurationTab;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.dialogs.ResourceListSelectionDialog;

public class PapyrusrtLaunchTab extends AbstractLaunchConfigurationTab implements ModifyListener {
	
	private Text modelFile;
	private Button modelFileButton;	
		
	private Label scriptFileLabel;
	private Text scriptFile;
	private Button scriptFileButton;
	
	private Combo mode;
	
    @Override
    public void createControl(Composite parent) {

        Composite comp = new Group(parent, SWT.BORDER);
        setControl(comp);
        
        Label modelFileLabel = new Label(comp, SWT.NONE);
        modelFileLabel.setText("Model File:");
        GridDataFactory.swtDefaults().applyTo(modelFileLabel);
        
        modelFile = new Text(comp, SWT.BORDER);
        modelFile.addModifyListener(this);
        modelFile.setEditable(false);
        GridDataFactory.fillDefaults().grab(true, false).applyTo(modelFile);
        
        modelFileButton = createPushButton(comp, "&Browse...", null); //$NON-NLS-1$
        modelFileButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				browseModelFiles();
			}
		});
        GridDataFactory.swtDefaults().applyTo(modelFileButton);
        
        Label dummyLabel = new Label(comp, SWT.NONE);
        dummyLabel.setText("");
        GridDataFactory.swtDefaults().applyTo(dummyLabel);
        
        Label modeLabel = new Label(comp, SWT.NONE);
        modeLabel.setText("Mode:");
        GridDataFactory.swtDefaults().applyTo(modeLabel);

        mode = new Combo (comp, SWT.READ_ONLY);
        mode.setItems("Interactive", "Script");
        mode.addModifyListener(this);
        GridDataFactory.fillDefaults().grab(true, false).applyTo(mode);

        mode.addSelectionListener(new SelectionListener() {
			
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				if(mode.getSelectionIndex() == 0) {
					scriptFileButton.setVisible(false);
					scriptFileLabel.setVisible(false);
					scriptFile.setVisible(false);
				} else {
					scriptFileButton.setVisible(true);
					scriptFileLabel.setVisible(true);
					scriptFile.setVisible(true);
				}
			}
			
			@Override
			public void widgetDefaultSelected(SelectionEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
        
        scriptFileLabel = new Label(comp, SWT.NONE);
        scriptFileLabel.setText("Script File:");
        GridDataFactory.swtDefaults().applyTo(scriptFileLabel);
        
        scriptFile = new Text(comp, SWT.BORDER);
        scriptFile.addModifyListener(this);
        scriptFile.setEditable(false);
        GridDataFactory.fillDefaults().grab(true, false).applyTo(scriptFile);
        
        scriptFileButton = createPushButton(comp, "&Browse...", null); //$NON-NLS-1$
        scriptFileButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				browseScriptFiles();
			}
		});
        GridDataFactory.swtDefaults().applyTo(modelFileButton);
        
        Label dummyLabel2 = new Label(comp, SWT.NONE);
        dummyLabel2.setText("");
        GridDataFactory.swtDefaults().applyTo(dummyLabel2);
        
        GridLayoutFactory.swtDefaults().numColumns(2).applyTo(comp);
    }

	private void browseModelFiles() {
		ResourceListSelectionDialog dialog = new ResourceListSelectionDialog(getShell(), ResourcesPlugin.getWorkspace().getRoot(), IResource.FILE);
		dialog.setTitle("Model File"); //$NON-NLS-1$
		dialog.setMessage("Select Model File"); //$NON-NLS-1$
		if (dialog.open() == Window.OK) {
			Object[] files = dialog.getResult();
			IFile file = (IFile) files[0];
			modelFile.setText(URI.createPlatformResourceURI(file.getFullPath().toString(), true).toString());
			updateLaunchConfigurationDialog();
		}
	}
	
	private void browseScriptFiles() {
		ResourceListSelectionDialog dialog = new ResourceListSelectionDialog(getShell(), ResourcesPlugin.getWorkspace().getRoot(), IResource.FILE);
		dialog.setTitle("Script File"); //$NON-NLS-1$
		dialog.setMessage("Select Script File"); //$NON-NLS-1$
		if (dialog.open() == Window.OK) {
			Object[] files = dialog.getResult();
			IFile file = (IFile) files[0];
			scriptFile.setText(URI.createPlatformResourceURI(file.getFullPath().toString(), true).toString());
			updateLaunchConfigurationDialog();
		}
	}
	
    @Override
    public void setDefaults(ILaunchConfigurationWorkingCopy configuration) {
    }

    @Override
    public void initializeFrom(ILaunchConfiguration configuration) {
        try {
        	modelFile.setText(configuration.getAttribute(PapyrusrtLaunchConfigurationAttributes.ATTR_MODEL_FILE, ""));
        	scriptFile.setText(configuration.getAttribute(PapyrusrtLaunchConfigurationAttributes.ATTR_SCRIPT_FILE, ""));
        	mode.select(configuration.getAttribute(PapyrusrtLaunchConfigurationAttributes.ATTR_MODE, 0));
        	
        	if(mode.getSelectionIndex() == 0) {
			scriptFileButton.setVisible(false);
			scriptFileLabel.setVisible(false);
			scriptFile.setVisible(false);
		} else {
			scriptFileButton.setVisible(true);
			scriptFileLabel.setVisible(true);
			scriptFile.setVisible(true);
		}
        	} catch (CoreException e) {}
    }

    @Override
    public void performApply(ILaunchConfigurationWorkingCopy configuration) {
    		configuration.setAttribute(PapyrusrtLaunchConfigurationAttributes.ATTR_MODE, mode.getSelectionIndex()); 
    		configuration.setAttribute(PapyrusrtLaunchConfigurationAttributes.ATTR_MODEL_FILE, modelFile.getText());
	    	configuration.setAttribute(PapyrusrtLaunchConfigurationAttributes.ATTR_SCRIPT_FILE, scriptFile.getText());
	    	
	    	if(mode.getSelectionIndex() == 0)
	    		configuration.setAttribute(PapyrusrtLaunchConfigurationAttributes.ATTR_SCRIPT_FILE, "");
	}

    @Override
    public String getName() {
        return "Configuration";
    }

	@Override
	public void modifyText(ModifyEvent arg0) {
		updateLaunchConfigurationDialog();
	}
}
