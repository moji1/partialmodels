package ca.jahed.papyrusrt.partialmodels;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.papyrus.infra.core.resource.IModel;
import org.eclipse.papyrus.infra.core.resource.ModelSet;
import org.eclipse.papyrus.infra.core.sashwindows.di.service.IPageManager;
import org.eclipse.papyrus.infra.core.services.ServiceException;
import org.eclipse.papyrus.infra.emf.utils.ServiceUtilsForEObject;
import org.eclipse.papyrus.infra.gmfdiag.css.CSSShapeImpl;
import org.eclipse.papyrus.infra.gmfdiag.css.notation.CSSDiagram;
import org.eclipse.papyrus.infra.gmfdiag.css.resource.CSSNotationModel;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.StateMachine;

public class PartialModel extends CSSNotationModel implements IModel {

	private static final int ACTIVE_FILL_COLOR = 8905185;
	
	private HashMap<String, CSSShapeImpl> states;
	private boolean undo;
	
	private DebuggerListener debuggerListener;
	
	public PartialModel() {
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	public void loadModel(URI uriWithoutExtension) {
		// TODO Auto-generated method stub
		super.loadModel(uriWithoutExtension);
		
		states = new HashMap<>();
		for(EObject obj : getResource().getContents()) {
			if(obj instanceof CSSDiagram) {
				CSSDiagram di = (CSSDiagram) obj;
				states.putAll(findStates(di));
			}
		}
		
		debuggerListener = new DebuggerListener(this);
		debuggerListener.start();
	}
	
	

	@Override
	public void unload() {
		// TODO Auto-generated method stub
		super.unload();
		
		if(debuggerListener != null)
			debuggerListener.abort();
	}


	public HashMap<String, CSSShapeImpl> findStates(CSSDiagram di) {
		HashMap<String, CSSShapeImpl> states = new HashMap<>();
		EList<View> children = di.getPersistedChildren();
		findStatesHelper(children, states);
		return states;
	}
	
	public void findStatesHelper(EList<View> children, HashMap<String, CSSShapeImpl> states) {
		for(View v : children) {
			if(v instanceof CSSShapeImpl
					&& v.getElement() instanceof State) {
				states.put(((State)v.getElement()).getQualifiedName(), (CSSShapeImpl)v);
			}
			
			if(v.getPersistedChildren() != null) {
				findStatesHelper(v.getPersistedChildren(), states);
			}
		}
	}

	public void goTo(String capsule, String state) throws ServiceException {
		
		CSSShapeImpl view = null;
		for(String key : states.keySet()) { // should use fqn
			if(key.contains(capsule) && key.endsWith(state)) {
				view = states.get(key);
				break;
			}
		}
		
		if(view == null)
			return;
		
		CSSDiagram di = (CSSDiagram) view.getDiagram();
		if(di == null)
			return;
		
		IPageManager mgr = ServiceUtilsForEObject.getInstance().getIPageManager(di.getElement());
		if(!mgr.isOpen(di))
			mgr.openPage(di);
		mgr.selectPage(di);
		
		
		TransactionalEditingDomain domain = getModelManager().getTransactionalEditingDomain();
		if(undo) {
			domain.getCommandStack().undo();
			undo = false;
		}
		
		final CSSShapeImpl stateView = view;
	    domain.getCommandStack().execute(new RecordingCommand(domain) {
			
			@Override
			protected void doExecute() {
				stateView.setFillColor(ACTIVE_FILL_COLOR);
				undo = true;
			}
		});
	}
	
	public void reset() {
		ModelSet modelSet = getModelManager();
		if(modelSet != null) {
			TransactionalEditingDomain domain = modelSet.getTransactionalEditingDomain();
			if(undo) {
				domain.getCommandStack().undo();
				undo = false;
			}
		}
	}

	public class DebuggerListener extends Thread {
		private ServerSocket serverSocket;
		private Socket socket;
		private BufferedInputStream in;
		private BufferedOutputStream out;
		private PartialModel model;
		private boolean abort;
		
		public DebuggerListener(PartialModel model) {
			this.model = model;
		}
		
		public void run() {
			while(!abort) {
				try {
					execute();
				} catch(Exception e) {
					e.printStackTrace();
					reset();
					try {
						Thread.sleep(100);
					} catch (InterruptedException e1) {
					}
				}
			}
		}
		
		public void execute() throws Exception {
			serverSocket = new ServerSocket();
			serverSocket.setReuseAddress(true);
			serverSocket.bind(new InetSocketAddress(6969));
			socket = serverSocket.accept();
			serverSocket.close();
			
			in = new BufferedInputStream(socket.getInputStream());
			out = new BufferedOutputStream(socket.getOutputStream());
			byte[] data = new byte[128];
			
			int read;
			while(!abort && socket.isConnected() && (read = in.read(data)) != -1) {
				String msg = new String(data, 0, read).trim();
				
				if(msg.startsWith("reset")) {
					model.reset();
				} else if(msg.startsWith("goto")){
					String tokens[] = msg.split(",");
					String capsule = tokens[1];
					String state = tokens[2];
					model.goTo(capsule, state); // should be fqn
				}
			}
			
			reset();
		}
		
		public void abort() {
			abort=true;
			try {
				if(serverSocket != null)
					serverSocket.close();
			} catch (IOException e) {
			}
			try {
				if(socket != null)
					socket.close();
			} catch (IOException e) {
			}
		}
	}

	
}
