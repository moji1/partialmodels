set(CMAKE_C_COMPILER "/usr/bin/clang-6.0")
set(CMAKE_CXX_COMPILER "/usr/bin/clang++-6.0")

set(ANTLR_EXECUTABLE "/home/kjahed/moji/antlr-4.7.2-complete.jar")
set(ANTLR4CPP_ROOT "/home/kjahed/moji/antlr4cpp")

set(UMLRTS_ROOT "/home/kjahed/moji/plugins-tcp/org.eclipse.papyrusrt.rts_1.0.0.201903201525/umlrts" CACHE STRING "")

set(PAPYRUSRT_PLUGINS "/home/kjahed/moji/plugins-tcp" CACHE STRING "")

set(CODEGEN_EXEC "/home/kjahed/moji/partialmodels/tools/umlrtgen.jar" )
set(TRANSFORMER_EXEC "/home/kjahed/moji/partialmodels/tools/umlrttransformer.jar"  )
set(REFINEMENT_SCRIPT "/home/kjahed/moji/partialmodels/transformation/script/RefineForPMD.eol" )

set(INPUT_MODEL "/home/kjahed/moji/TrafficLight/TrafficLight.uml")
set(TOP_CAPSULE "Top")
set(GENERATED_FILES
ControlProtocol.cc
TrafficLightProtocol.cc
TrafficLightDriver.cc
Controller.cc
UserConsole.cc
Top.cc
TopControllers.cc
TopMain.cc
dbgAgent.cc
dbgProtocol.cc
)

