//
//  ExecContext.hpp
//  pmecore
//
//  Created by Mojtaba Bagherzadeh on 2018-09-14.
//

#ifndef ExecContext_hpp
#define ExecContext_hpp
#include <iostream>
#include "ExecRuleTypes.hpp"
#include "../util/Heap.hpp"
#include "PMDTypes.hpp"
#include <boost/circular_buffer.hpp>
#include "umlrtmessage.hh"
#include <iomanip>
namespace pmd{

class ExecContextOfCapsule{
public:
    //ExecContextOfCapsule(int execHistoryBufferSize, int userDecesionHistoryBufferSize);
    ExecContextOfCapsule();
    void setExecHistoryBufferCapacity(int n);
    void setUserDecesionHistoryBufferCapacity(int n);
    void setCapsuleInstanceID(int id);
    int getCapsuleInstance();
    void setExecLocation(ExecLocationType type, const char *  state, const char *  capsule);
    ExecLocation getExecLocation();
    void setCapsuleType(const char * capsuleInstance);
    std::string getCapsuleType();
    void setCapsuleHeap(Heap * heap);
    Heap * getCapsuleHeap();
    void setlastMsg(const UMLRTMessage * msg);
    Message getLastMsg();
    void setLastTransition(const char * transName);
    std::string getLastTransition();
    void appendpostponedActionCodes(std::function<bool(const UMLRTMessage * msg)>);
    void clearPostponedActionCodes();
    void executePostponedActionCodes();
    void addInputMessages(std::string portName, std::string msgName, std::map<std::string,std::string> paramsWithType);
    void addOutPutMessages(std::string portName, std::string msgName, std::map<std::string,std::string> paramsWithType);
    void setCallBackMethodSendMessage(std::function<int(Message)> pSendMessage);
    std::function<int(Message)>  getCallBackMethodSendMessage();
    void setCallBackMethodSendInternalMessage(std::function<int(Message)> pSendInternalMessage);
    std::function<int(Message)>  getCallBackMethodSendInternalMessage();
    void addPossibleExecPath(std::string source, std::string target, std::string transtion, std::string signal);
    void clearPossibleExecPath();
    std::vector<ExecutionStep> getPossibleExecPath();
    void setPartialityPattern(PartialityPattern partialityPattern);
    PartialityPattern getPartialityPattern();
    std::map<std::string,std::string> getMessageParam(std::string portName,std::string messageName);
    std::string printMessageParamstoStr(Message msg);
    bool checkUserDecesion(ExecutionStep path);
    ExecutionStep getSelectedPath();
    void setSelectedPath(pmd::ExecutionStep executionStep);
    void setSelectedPath(int pathId);
    void showContext();
    void showOptions();
    void showDecesions();
    void showVar();
    void showVar(std::string name);
    void showTrace();
    void showTrace(int cnt);
    void showConnections();
    void showDecesions(int cnt);
    void showInputMsgs();
    void showOutputMsgs();
    void showLastMsg();
    void showMessageMap(MessagesMap messageMap);
    void saveEffect(std::string effect);
    std::string getEffect();
    //void printVar(std::string name);

    static ExecContextOfCapsule* allContexts[1000];
    static int allContexts_idx;
    
private:
    int capsuleInstanceID;
    ExecLocation execLocation;
    std::string capsuleType;
    PartialityPattern partialityPattern;
    Heap * capsuleHeap;
    //Heap scriptHeap;
    Message lastMsg;
    std::string lastTransition;
    const UMLRTMessage * lastUMLRTMsg;
    std::function<int(Message)> pSendMessage;
    std::function<int(Message)> pSendInternalMessage;
    std::vector<std::function<bool(const UMLRTMessage * msg)>> postponedActionCodes;
    boost::circular_buffer<ExecLocation> execHistory;
    boost::circular_buffer<ExecutionStep>    userDecesionHistory;
    MessagesMap inputMessages;
    MessagesMap outputMessages;
    std::vector<ExecutionStep> possibleExecPaths;
    void appendExecHistory(ExecLocation execHistory);
    void appenduserDecesionHistory(ExecutionStep userDecision);
    Message convertUMLRTMessageToMessage(const UMLRTMessage *msg);
    ExecutionStep selectedPath;
    bool runEntry;
    bool runExit;
    EffectMap effects;
};

// should make this dynamic
}

#endif /* ExecContext_hpp */
